terraform {
  required_version = "~> 1.0"
  required_providers {
    aws = {
      source  = "hashicorp/aws"
      version = "~> 4.0"
    }
  }
}


resource "aws_vpc" "dev-vpc" {
  cidr_block = var.vpc_cidr

  tags = {
    Name = "${var.project_name}-vpc"
    Demo = "Terraform"
  }
}

data "aws_availability_zones" "available_zones" {}


resource "aws_subnet" "dev-subnet-1" {
  vpc_id            = aws_vpc.dev-vpc.id
  cidr_block        = var.public-subnet-az1
  availability_zone = data.aws_availability_zones.available_zones.names[0]

  tags = {
    Name = "Dev-Subnet1"
    Type = "Public"
  }
}

resource "aws_subnet" "dev-subnet-2" {
  vpc_id            = aws_vpc.dev-vpc.id
  cidr_block        = var.public-subnet-az2
  availability_zone = data.aws_availability_zones.available_zones.names[1]

  tags = {
    Name = "Dev-Subnet2"
    Type = "Public"
  }
}

resource "aws_internet_gateway" "dev-gateway" {
  vpc_id = aws_vpc.dev-vpc.id

  tags = {
    "Name"  = "Dev-Gateway"
    "Owner" = "DevTeam"
  }
}


resource "aws_route_table" "dev-rt-1" {
  vpc_id = aws_vpc.dev-vpc.id

  route {
    cidr_block = "0.0.0.0/0"
    gateway_id = aws_internet_gateway.dev-gateway.id
  }

  tags = {
    Name = "Dev-Public"
  }
}

resource "aws_route_table_association" "dev-rta-1" {
  subnet_id      = aws_subnet.dev-subnet-1.id
  route_table_id = aws_route_table.dev-rt-1.id
}

resource "aws_route_table_association" "dev-rta-2" {
  subnet_id      = aws_subnet.dev-subnet-2.id
  route_table_id = aws_route_table.dev-rt-1.id
}


resource "aws_security_group" "dev-webserver" {
  name        = var.sg_name
  description = var.sg_description
  vpc_id      = aws_vpc.dev-vpc.id

  ingress {
    description = "SSH from anywhere"
    from_port   = 22
    to_port     = 22
    protocol    = "tcp"
    cidr_blocks = [var.ip-address]
  }

  ingress {
    description = "80 from anywhere"
    from_port   = 80
    to_port     = 80
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }

  egress {
    from_port   = 0
    to_port     = 0
    protocol    = -1
    cidr_blocks = [var.public-address]
  }

  tags = {
    Name = "Allow traffic"
  }
}

resource "aws_instance" "dev-web" {
  ami                    = var.ami_id
  instance_type          = var.instance_size
  key_name               = var.key_name
  subnet_id              = aws_subnet.dev-subnet-1.id
  vpc_security_group_ids = [aws_security_group.dev-webserver.id]

  associate_public_ip_address = true

  #userdata
  user_data = <<EOF
    #!/bin/bash
    apt-get update
    apt-get install -y nginx
    echo "<html><body><h1>Welcome to Nginx!</h1></body></html>" > /var/www/html/index.html
    systemctl start nginx
    EOF

  tags = {
    Name = "Dev"
  }
}


