
variable "instance_size" {
  type = string
}

variable "key_name" {
  type = string
}

variable "project_name" {
  type = string
}

variable "vpc_cidr" {
  type = string
}

variable "public-subnet-az1" {
  type = string
}

variable "public-subnet-az2" {
  type = string
}


variable "sg_name" {
  type = string
}

variable "sg_description" {
  type = string
}

variable "ip-address" {
  type = string
}

variable "public-address" {
  type = string
}


variable "ami_id" {
  type = string
}
