vpc_cidr          = "100.0.0.0/16"
project_name  = "exercise1"
public-subnet-az1       = "100.0.1.0/24"
public-subnet-az2       = "100.0.2.0/24"
sg_name        = "exercise1 security group"
sg_description = "allow access on ports 80 and 22"
ip-address     = "0.0.0.0/0"
public-address    = "0.0.0.0/0"
ami_id        = "ami-052efd3df9dad4825"
instance_size = "t2.micro"
key_name      = "new"
