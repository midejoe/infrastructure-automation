output "vpc_id" {
  value = aws_vpc.dev-vpc.id
}

output "subnet1_id" {
  value = aws_subnet.dev-subnet-1.id
}

output "subnet2_id" {
  value = aws_subnet.dev-subnet-2.id
}

output "web_instance_public_ip" {
  value = aws_instance.dev-web.public_ip
}
